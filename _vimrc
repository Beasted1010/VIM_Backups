
" NOTE: The colon is left out for each of these commands, it is OPTIONAL when running scripts because they are assumed to be normal mode commands


" This is to prevent some options being set as a sideeffect of compatible
set nocompatible



" Set my color scheme
colorscheme desert

" Enable syntax highlighting
syntax on

" For plugin management stuff
execute pathogen#infect()
" call pathogen#runtime_append_all_bundles()
" set runtimepath^=C:\Program Files (x86)\Vim\vim80\runtime\bundle\NERD_tree\autoload\nerdtree.vim

" Nerdtree configurations
" Below line is mapping for openning the file navigation window
map <C-n> :NERDTreeToggle<CR>
" Below 2 lines allow me to automatically open NERDTree if no file was specified
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
" Below line allows me to cloes NERDTree automatically if it is the last thing open in the editor
" autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif



set smartindent
" TAB CONFIGURATIONS
set shiftwidth=4
set tabstop=4
set expandtab
set smarttab

" Minimal number of screen lines to keep above and below the cursor.
set scrolloff=999

" Fixing a scroll bug with buffer not being drawn when scrolling pages
set ttyscroll=10

" Make backspace work like other editors
set backspace=indent,eol,start

" Turn on line numbers
"set number

" Show line number, cursor position
set ruler

" To save, press ctrl-s.
nmap <c-s> :w<CR>
imap <c-s> <Esc>:w<CR>a

" Search as you type.
set incsearch

" Ignore case when searching.
set ignorecase

" Show autocomplete menus.
set wildmenu

" Error bells are displayed visually.
set visualbell






""""""""""""""""""""""" SHORT CUTS
map <F5> :call CurtineIncSw()<CR>




